# HelioSystem

The center of your organization's universe.

## Installation
We recommend working on HelioSystem through [HelioCenter](https://gitlab.com/heliosystem/heliocenter). HelioSystem by itself won't do much.

```
git clone --recurse-submodules -j4 https://gitlab.com/heliosystem/heliocenter.git
cd heliocenter
git submodule update --init --recursive

# and then make sure you have the latest updates to HelioSystem
cd engines/heliosystem
git pull origin master
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
