module Heliosystem
  class SubfieldValue < ApplicationRecord
    belongs_to :subfield
  end
end
