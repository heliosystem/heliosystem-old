module Heliosystem
  class SubrelationPair < ApplicationRecord
    belongs_to :subrelation
    belongs_to :parent, polymorphic: true
    belongs_to :child, polymorphic: true
  end
end
