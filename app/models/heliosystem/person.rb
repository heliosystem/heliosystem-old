module Heliosystem
  class Person < ApplicationRecord
    include Customizable
    include Taggable
    belongs_to :universe
    belongs_to :user
    has_many :memberships
    has_many :organizations, through: :memberships
    has_many :timepunches
    accepts_nested_attributes_for :user

    def name
      user.first_name + ' ' + user.last_name
    end
  end
end
