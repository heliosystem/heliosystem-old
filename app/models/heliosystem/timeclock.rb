module Heliosystem
  class Timeclock < ApplicationRecord
    include Customizable
    include Taggable
    belongs_to :organization
    has_many :timepunches

    def hours
      timepunches.closed.map(&:hours).reduce(0) { |v, a| a + v }
    end

    def universe
      organization.universe
    end
  end
end
