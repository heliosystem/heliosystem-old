module Heliosystem
  class Tag < ApplicationRecord
    extend FriendlyId
    friendly_id :name, use: :slugged
    has_many :taggings
    belongs_to :universe
    belongs_to :organization, optional: true

    def taggables
      taggings.map(&:taggable)
    end
  end
end
