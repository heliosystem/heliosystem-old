//= require rails-ujs

$(function () {
  $('[data-toggle="popover"]').popover();
  $('[data-modal-target]').click((e) => {
    console.log(e);
    $('#' + e.currentTarget.dataset.modalTarget).modal('show')
  });
})
