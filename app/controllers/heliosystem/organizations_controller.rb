require_dependency "heliosystem/application_controller"

module Heliosystem
  class OrganizationsController < ApplicationController
    before_action :set_breadcrumbs
    before_action :set_organization, only: [:show, :edit, :update, :destroy]

    def index
      @organizations = current_universe.organizations
      @breadcrumbs << [universe_organizations_path(current_universe), 'Organizations']
    end

    # GET /organizations/1
    def show
    end

    # GET /organizations/new
    def new
      @organization = current_universe.organizations.build
    end

    # GET /organizations/1/edit
    def edit
    end

    # POST /organizations
    def create
      @organization = current_universe.organizations.build(organization_params)

      if @organization.save
        redirect_to [@organization.universe, @organization], notice: 'Organization was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /organizations/1
    def update
      if @organization.update(organization_params)
        redirect_to [@organization.universe, @organization], notice: 'Organization was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /organizations/1
    def destroy
      @organization.destroy
      redirect_to current_universe, notice: 'Organization was successfully destroyed.'
    end

    private
      def set_breadcrumbs
        @breadcrumbs << [current_universe, current_universe.name]
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_organization
        @organization = current_universe.organizations.friendly.find(params[:id])
        @breadcrumbs << [universe_organization_path(@organization.universe, @organization), @organization.name] if @organization.id
      end

      # Only allow a trusted parameter "white list" through.
      def organization_params
        params.require(:organization).permit(:name, :description, :color, :accounts_enabled, :pages_enabled, :timeclocks_enabled, person_ids: [])
      end
  end
end
