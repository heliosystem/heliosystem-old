require_dependency "heliosystem/application_controller"

module Heliosystem
  class SubfieldsController < ApplicationController
    before_action :set_breadcrumbs
    before_action :set_subfield, only: [:show, :edit, :update, :destroy]

    # GET /subfields
    def index
      @subfields = current_universe.subfields.all
    end

    # GET /subfields/1/edit
    def edit
    end

    # POST /subfields
    def create
      @subfield = current_universe.subfields.build(subfield_params)

      if @subfield.save
        redirect_to universe_subfields_path(@subfield.universe), notice: 'Subfield was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /subfields/1
    def update
      if @subfield.update(subfield_params)
        redirect_to universe_subfields_path(@subfield.universe), notice: 'Subfield was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /subfields/1
    def destroy
      @subfield.destroy
      redirect_to universe_subfields_path(current_universe), notice: 'Subfield was successfully destroyed.'
    end

    private

      def set_breadcrumbs
        @breadcrumbs << [current_universe, current_universe.name]
        @breadcrumbs << [universe_subfields_path(current_universe), 'Custom Fields Setup']
      end
      # Use callbacks to share common setup or constraints between actions.
      def set_subfield
        @subfield = current_universe.subfields.find(params[:id])
        @breadcrumbs << [universe_subfield_path([@subfield.universe, @subfield]), @subfield.label]
      end

      # Only allow a trusted parameter "white list" through.
      def subfield_params
        params.require(:subfield).permit(:label, :format, :app, :universe_id)
      end
  end
end
