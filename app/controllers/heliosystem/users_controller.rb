require_dependency "heliosystem/application_controller"

module Heliosystem
  class UsersController < ApplicationController
    skip_before_action :check_for_user, only: [:create]
    
    def create
      @user = User.new(user_params)
      @user.email.downcase!

      if @user.save
        flash[:notice] = "Account created successfully!"
        redirect_to root_path
      else
        flash.now.alert = "Oops, couldn't create account. Please make sure you are using a valid email and password and try again."
        redirect_to root_path
      end
    end

    private

    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :password, :password_confirmation)
    end
  end
end
