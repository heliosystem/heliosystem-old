require_dependency "heliosystem/application_controller"

module Heliosystem
  class SettingsController < ApplicationController
    before_action :set_breadcrumbs, except: [:destroy]
    before_action :set_setting, only: [:update, :destroy]

    # GET /settings
    def index
      @settings = current_container.settings.all
      @blank_setting = current_container.settings.build()
    end

    # POST /settings
    def create
      @setting = current_container.settings.build(setting_params)

      if @setting.save
        redirect_to current_container_settings_path, notice: 'Setting was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /settings/1
    def update
      if @setting.update(setting_params)
        redirect_to current_container_settings_path, notice: 'Setting was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /settings/1
    def destroy
      @setting.destroy
      redirect_to current_container_settings_path, notice: 'Setting was successfully destroyed.'
    end

    private
      def set_breadcrumbs
        @breadcrumbs << [current_universe, current_universe.name]
        if current_organization
          @breadcrumbs << [[current_universe, current_organization], current_organization.name]
          @breadcrumbs << [universe_organization_settings_path(current_universe, organization_id: current_organization.slug), 'Settings']
        else
          @breadcrumbs << [universe_settings_path(current_universe), 'Settings']
        end
      end
      # Use callbacks to share common setup or constraints between actions.
      def set_setting
        @setting = Setting.find(params[:id])
        if current_organization
          @breadcrumbs << [universe_organization_setting_path(current_universe, @setting, organization_id: current_organization.slug), @setting.key] if @setting.id
        else
          @breadcrumbs << [universe_setting_path(@setting.universe, @setting), @setting.key] if @setting.id
        end
      end

      # Only allow a trusted parameter "white list" through.
      def setting_params
        params.require(:setting).permit(:key, :value)
      end

      def current_container
        current_organization || current_universe
      end

      def current_container_path
        [current_universe, current_organization].filter(&:present?)
      end

      def current_container_settings_path
        if current_organization
          universe_organization_settings_path(current_organization, universe_id: current_universe.slug)
        else
          universe_settings_path(current_universe)
        end
      end
  end
end
