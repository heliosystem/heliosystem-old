require_dependency "heliosystem/application_controller"

module Heliosystem
  class TagsController < ApplicationController
    before_action :set_breadcrumbs
    before_action :set_tag, only: [:show, :edit, :update, :destroy]

    # GET /tags
    def index
      if current_organization
        @tags = current_organization.tags
      else
        @tags = current_universe.top_level_tags
      end
    end

    # GET /tags/1
    def show
    end

    # GET /tags/new
    def new
      @tag = current_container.tags.build
    end

    # GET /tags/1/edit
    def edit
    end

    # POST /tags
    def create
      @tag = current_container.tags.build(tag_params)
      @tag.universe ||= current_universe

      if @tag.save
        redirect_to [current_container_path, @tag].flatten, notice: 'Tag was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /tags/1
    def update
      if @tag.update(tag_params)
        redirect_to [current_container_path, @tag].flatten, notice: 'Tag was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /tags/1
    def destroy
      @tag.destroy
      redirect_to current_container_path, notice: 'Tag was successfully destroyed.'
    end

    private
      def set_breadcrumbs
        @breadcrumbs << [current_universe, current_universe.name]
        if current_organization
          @breadcrumbs << [[current_universe, current_organization], current_organization.name]
          @breadcrumbs << [universe_organization_tags_path(current_universe, organization_id: current_organization.slug), 'Tags']
        else
          @breadcrumbs << [universe_tags_path(current_universe), 'Tags']
        end
      end
      # Use callbacks to share common setup or constraints between actions.
      def set_tag
        @tag = Tag.friendly.find(params[:id])
        if current_organization
          @breadcrumbs << [universe_organization_tag_path(@tag.universe, @tag, organization_id: current_organization.slug), @tag.name] if @tag.id
        else
          @breadcrumbs << [universe_tag_path(@tag.universe, @tag), @tag.name] if @tag.id
        end
      end

      def current_container
        current_organization || current_universe
      end

      def current_container_path
        [current_universe, current_organization].filter(&:present?)
      end

      # Only allow a trusted parameter "white list" through.
      def tag_params
        params.require(:tag).permit(:name, :description, :color)
      end
  end
end
