$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "heliosystem/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "heliosystem"
  spec.version     = Heliosystem::VERSION
  spec.authors     = [""]
  spec.email       = [""]
  spec.homepage    = "https://gitlab.com/heliosystem/heliosystem"
  spec.summary     = "HelioSystem Team Management System"
  spec.description = "HelioSystem Team Management System"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = ""
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 6.0.0"
  spec.add_dependency "webpacker", "~> 4.0"
  spec.add_dependency "sass-rails"
  spec.add_dependency "turbolinks", "~> 5"
  spec.add_dependency "bcrypt"
  spec.add_dependency "asciidoctor"
  
  spec.add_dependency "simple_form"
  spec.add_dependency "friendly_id"
  spec.add_dependency "color"
  spec.add_dependency "money"

  spec.add_development_dependency "pg"
  spec.add_development_dependency "sqlite3"
end
