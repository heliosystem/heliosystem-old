Rails.application.config.assets.precompile += %w( heliosystem/heliosystem.png heliosystem/heliosystem-inverse.png heliosystem/application.js )
Rails.application.config.assets.paths << Rails.root.join('engines/heliosystem/app/assets/javascripts')
