require 'test_helper'

module Heliosystem
  class SubrelationsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @subrelation = heliosystem_subrelations(:one)
    end

    test "should get index" do
      get subrelations_url
      assert_response :success
    end

    test "should get new" do
      get new_subrelation_url
      assert_response :success
    end

    test "should create subrelation" do
      assert_difference('Subrelation.count') do
        post subrelations_url, params: { subrelation: { child: @subrelation.child, format: @subrelation.format, label: @subrelation.label, parent: @subrelation.parent, universe_id: @subrelation.universe_id } }
      end

      assert_redirected_to subrelation_url(Subrelation.last)
    end

    test "should show subrelation" do
      get subrelation_url(@subrelation)
      assert_response :success
    end

    test "should get edit" do
      get edit_subrelation_url(@subrelation)
      assert_response :success
    end

    test "should update subrelation" do
      patch subrelation_url(@subrelation), params: { subrelation: { child: @subrelation.child, format: @subrelation.format, label: @subrelation.label, parent: @subrelation.parent, universe_id: @subrelation.universe_id } }
      assert_redirected_to subrelation_url(@subrelation)
    end

    test "should destroy subrelation" do
      assert_difference('Subrelation.count', -1) do
        delete subrelation_url(@subrelation)
      end

      assert_redirected_to subrelations_url
    end
  end
end
