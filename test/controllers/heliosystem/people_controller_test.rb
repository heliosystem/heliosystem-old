require 'test_helper'

module Heliosystem
  class PeopleControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @person = heliosystem_people(:one)
    end

    test "should get index" do
      get people_url
      assert_response :success
    end

    test "should get new" do
      get new_person_url
      assert_response :success
    end

    test "should create person" do
      assert_difference('Person.count') do
        post people_url, params: { person: { universe_id: @person.universe_id, user_id: @person.user_id } }
      end

      assert_redirected_to person_url(Person.last)
    end

    test "should show person" do
      get person_url(@person)
      assert_response :success
    end

    test "should get edit" do
      get edit_person_url(@person)
      assert_response :success
    end

    test "should update person" do
      patch person_url(@person), params: { person: { universe_id: @person.universe_id, user_id: @person.user_id } }
      assert_redirected_to person_url(@person)
    end

    test "should destroy person" do
      assert_difference('Person.count', -1) do
        delete person_url(@person)
      end

      assert_redirected_to people_url
    end
  end
end
