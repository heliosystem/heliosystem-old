require 'test_helper'

module Heliosystem
  class SubfieldsControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers

    setup do
      @subfield = heliosystem_subfields(:one)
    end

    test "should get index" do
      get subfields_url
      assert_response :success
    end

    test "should get new" do
      get new_subfield_url
      assert_response :success
    end

    test "should create subfield" do
      assert_difference('Subfield.count') do
        post subfields_url, params: { subfield: { app_id: @subfield.app_id, app_type: @subfield.app_type, label: @subfield.label, type: @subfield.type } }
      end

      assert_redirected_to subfield_url(Subfield.last)
    end

    test "should show subfield" do
      get subfield_url(@subfield)
      assert_response :success
    end

    test "should get edit" do
      get edit_subfield_url(@subfield)
      assert_response :success
    end

    test "should update subfield" do
      patch subfield_url(@subfield), params: { subfield: { app_id: @subfield.app_id, app_type: @subfield.app_type, label: @subfield.label, type: @subfield.type } }
      assert_redirected_to subfield_url(@subfield)
    end

    test "should destroy subfield" do
      assert_difference('Subfield.count', -1) do
        delete subfield_url(@subfield)
      end

      assert_redirected_to subfields_url
    end
  end
end
