require "application_system_test_case"

module Heliosystem
  class SubfieldsTest < ApplicationSystemTestCase
    setup do
      @subfield = heliosystem_subfields(:one)
    end

    test "visiting the index" do
      visit subfields_url
      assert_selector "h1", text: "Subfields"
    end

    test "creating a Subfield" do
      visit subfields_url
      click_on "New Subfield"

      fill_in "App", with: @subfield.app_id
      fill_in "App type", with: @subfield.app_type
      fill_in "Label", with: @subfield.label
      fill_in "Type", with: @subfield.type
      click_on "Create Subfield"

      assert_text "Subfield was successfully created"
      click_on "Back"
    end

    test "updating a Subfield" do
      visit subfields_url
      click_on "Edit", match: :first

      fill_in "App", with: @subfield.app_id
      fill_in "App type", with: @subfield.app_type
      fill_in "Label", with: @subfield.label
      fill_in "Type", with: @subfield.type
      click_on "Update Subfield"

      assert_text "Subfield was successfully updated"
      click_on "Back"
    end

    test "destroying a Subfield" do
      visit subfields_url
      page.accept_confirm do
        click_on "Destroy", match: :first
      end

      assert_text "Subfield was successfully destroyed"
    end
  end
end
