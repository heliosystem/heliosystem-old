require "application_system_test_case"

module Heliosystem
  class TimeclocksTest < ApplicationSystemTestCase
    setup do
      @timeclock = heliosystem_timeclocks(:one)
    end

    test "visiting the index" do
      visit timeclocks_url
      assert_selector "h1", text: "Timeclocks"
    end

    test "creating a Timeclock" do
      visit timeclocks_url
      click_on "New Timeclock"

      fill_in "Name", with: @timeclock.name
      fill_in "Organization", with: @timeclock.organization_id
      click_on "Create Timeclock"

      assert_text "Timeclock was successfully created"
      click_on "Back"
    end

    test "updating a Timeclock" do
      visit timeclocks_url
      click_on "Edit", match: :first

      fill_in "Name", with: @timeclock.name
      fill_in "Organization", with: @timeclock.organization_id
      click_on "Update Timeclock"

      assert_text "Timeclock was successfully updated"
      click_on "Back"
    end

    test "destroying a Timeclock" do
      visit timeclocks_url
      page.accept_confirm do
        click_on "Destroy", match: :first
      end

      assert_text "Timeclock was successfully destroyed"
    end
  end
end
