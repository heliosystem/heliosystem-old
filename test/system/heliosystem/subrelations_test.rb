require "application_system_test_case"

module Heliosystem
  class SubrelationsTest < ApplicationSystemTestCase
    setup do
      @subrelation = heliosystem_subrelations(:one)
    end

    test "visiting the index" do
      visit subrelations_url
      assert_selector "h1", text: "Subrelations"
    end

    test "creating a Subrelation" do
      visit subrelations_url
      click_on "New Subrelation"

      fill_in "Child", with: @subrelation.child
      fill_in "Format", with: @subrelation.format
      fill_in "Label", with: @subrelation.label
      fill_in "Parent", with: @subrelation.parent
      fill_in "Universe", with: @subrelation.universe_id
      click_on "Create Subrelation"

      assert_text "Subrelation was successfully created"
      click_on "Back"
    end

    test "updating a Subrelation" do
      visit subrelations_url
      click_on "Edit", match: :first

      fill_in "Child", with: @subrelation.child
      fill_in "Format", with: @subrelation.format
      fill_in "Label", with: @subrelation.label
      fill_in "Parent", with: @subrelation.parent
      fill_in "Universe", with: @subrelation.universe_id
      click_on "Update Subrelation"

      assert_text "Subrelation was successfully updated"
      click_on "Back"
    end

    test "destroying a Subrelation" do
      visit subrelations_url
      page.accept_confirm do
        click_on "Destroy", match: :first
      end

      assert_text "Subrelation was successfully destroyed"
    end
  end
end
