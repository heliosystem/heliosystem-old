class CreateHeliosystemSubrelations < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_subrelations, id: :uuid do |t|
      t.string :label
      t.string :format
      t.string :parent
      t.string :child
      t.uuid :universe_id

      t.timestamps
    end
    add_index :heliosystem_subrelations, [:universe_id, :parent]
    add_index :heliosystem_subrelations, [:universe_id, :label]
    add_index :heliosystem_subrelations, [:universe_id, :parent, :label], name: 'index_heliosystem_subrelation_full_query'
  end
end
