class CreateHeliosystemSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_settings, id: :uuid do |t|
      t.string :key
      t.string :value
      t.uuid :universe_id
      t.uuid :organization_id

      t.timestamps
    end
    add_index :heliosystem_settings, :universe_id
    add_index :heliosystem_settings, [:universe_id, :organization_id]
    add_index :heliosystem_settings, :key
  end
end
