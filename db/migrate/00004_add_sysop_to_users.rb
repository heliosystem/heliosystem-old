class AddSysopToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :heliosystem_users, :sysop, :boolean, default: false
  end
end
