class CreateHeliosystemTimeclocks < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_timeclocks, id: :uuid do |t|
      t.string :name
      t.string :slug
      t.uuid :organization_id

      t.timestamps
    end
    add_index :heliosystem_timeclocks, :slug
    add_index :heliosystem_timeclocks, [:organization_id, :slug], unique: true
  end
end
