class CreateHeliosystemSubrelationPairs < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_subrelation_pairs, id: :uuid do |t|
      t.uuid :subrelation_id
      t.string :parent_type
      t.uuid :parent_id
      t.string :child_type
      t.uuid :child_id

      t.timestamps
    end
    add_index :heliosystem_subrelation_pairs, [:subrelation_id, :parent_type, :parent_id], name: 'index_heliosystem_parent_subrelation'
    add_index :heliosystem_subrelation_pairs, [:subrelation_id, :child_type, :child_id], name: 'index_heliosystem_child_subrelation'
  end
end
