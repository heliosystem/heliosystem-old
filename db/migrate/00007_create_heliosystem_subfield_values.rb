class CreateHeliosystemSubfieldValues < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_subfield_values, id: :uuid do |t|
      t.uuid :subfield_id
      t.string :app_type
      t.uuid :app_id
      t.string :value

      t.timestamps
    end
    add_index :heliosystem_subfield_values, [:subfield_id, :app_type, :app_id], name: 'index_heliosystem_full_subfield_query'
    add_index :heliosystem_subfield_values, [:app_type, :app_id]
  end
end
