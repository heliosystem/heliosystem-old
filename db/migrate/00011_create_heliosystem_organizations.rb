class CreateHeliosystemOrganizations < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_organizations, id: :uuid do |t|
      t.string :name
      t.string :slug
      t.text :description
      t.uuid :universe_id

      t.timestamps
    end
    add_index :heliosystem_organizations, :slug, unique: true
    add_index :heliosystem_organizations, :universe_id
  end
end
