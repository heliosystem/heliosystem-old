class CreateHeliosystemPages < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_pages, id: :uuid do |t|
      t.string :title
      t.string :slug
      t.text :content
      t.uuid :organization_id

      t.timestamps
    end
    add_index :heliosystem_pages, [:organization_id, :slug], unique: true
  end
end
