class CreateHeliosystemMemberships < ActiveRecord::Migration[6.0]
  def change
    create_table :heliosystem_memberships, id: :uuid do |t|
      t.uuid :person_id
      t.uuid :organization_id

      t.timestamps
    end
    add_index :heliosystem_memberships, :person_id
    add_index :heliosystem_memberships, :organization_id
  end
end
