class AddNameToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :heliosystem_users, :first_name, :string
    add_column :heliosystem_users, :last_name, :string
  end
end
